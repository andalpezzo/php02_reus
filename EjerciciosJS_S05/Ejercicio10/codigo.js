var texto = prompt("Escribe un texto");
var txt = texto.replace(/ /g, "");

var resultado = esPalindromo(txt);

function esPalindromo(txt){
	if(txt.length <= 1){
		return "SI";
	} else {
		if(txt.charAt(0) == txt.charAt(txt.length - 1)){
			return esPalindromo(txt.substring(1,txt.length - 1));
		}else{
		    return "NO";
		}
	}
}

alert(resultado);
