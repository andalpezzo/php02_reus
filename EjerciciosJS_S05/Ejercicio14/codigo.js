//Declaración de variables
var num = 0;
var strNum;
var numLength;
var operacionClicada = false;
var signo = '';
var calculo;

//Función que coloca el número presionado
//El '+/-' y el '.' los he incluido aqui, porque era mas facil manejar su funcionalidad
function ponerNum(numero){
    if(document.getElementById("imput").value == 0 && !document.getElementById("imput").value.includes('.')){ //Condiciones que mira en caso de que sea 0 (No se haya escrito nada)
    	if(numero == '.') { //Si el boton que clicamos es el punto añdimos un '.' depues del 0
        	document.getElementById("imput").value = '0.';
        } else if (numero == '+/-') { //Si es la tecla es la de '+/-' no hacemos nada, porqe no puede haber 0 negativo
        	document.getElementById("imput").value = '';
        } else { //Si no ocurre ninguno de estos casos, sustituiremos el 0 por el numero clicado
        	document.getElementById("imput").value = numero;
    	}
    }else{
        if(operacionClicada == true) {  //Si una operacion ha sido clicada, sustituiremos el numero escrito por el nuevo numero
        	document.getElementById("imput").value = numero;
        	//Automaticamente ponemos la operacionClicada a false, para saber si ponemos numero o clicaron en otra operacion 
	        operacionClicada = false;
        } else {
	        //En el caso de que no se haya clicado en una operacion, se miraran estas condiciones.
	      	if(numero == '.' && document.getElementById("imput").value.includes('.')) { 			//Si el boton clicado es el punto y ya hay un punto, 
	        	document.getElementById("imput").value = document.getElementById("imput").value; 	//dejaremos el numero tal cual, porque no puede haber mas de un punto
	        } else if (numero == '+/-') { //Si el boton es el de negativo/positivo...
	        	if (!document.getElementById("imput").value.includes('-')) { //Miramos si ya no hay un '-'. Si no lo hay lo añadimos. 
	        		document.getElementById("imput").value = '-' + document.getElementById("imput").value;
	        	} else { //Si se vuleve a clicar al mismo boton y ya existe un '-', lo quitamos mediante substring.
	        		var numLength = document.getElementById("imput").value.toString().length;
	        		document.getElementById("imput").value = document.getElementById("imput").value.substring(1, numLength);
	        	}
	        } else { //Si solamente hemos clicado a un numero, lo añadiremos a el que ya hay
        		document.getElementById("imput").value += numero;
        	}
	    }
    }
}

//Funcion que controla los 3 bototnes rojos y borra lo que hay escrito
function borrar(boton) {
	switch (boton) { //Switch que controla que boton has aperetado
		//Si el boton que ha apretado es el de retroceder.
		//Haremos un substring y eliminaremos la el ultimo digito
		case 'retr':
			num = document.getElementById("imput").value;
			numLength = num.toString().length;
			document.getElementById("imput").value = strNum.substring(0, numLength - 1);
			if(numLength == 1) {
				document.getElementById("imput").value = 0;
			}
			break;
		//Si el  boton es el de CE, volveremos a poner el numero anteriror guardado
		case 'ce':
			document.getElementById("imput").value = num;
			break;
		//Si el boton es C lo borraremos todo poniendo un 0
		case 'c':
			document.getElementById("imput").value = 0;
			break;
		default:
			document.getElementById("imput").value;
		}
}

//Funcion que conrola la logica de la operaciones
function operaciones(operacion) {
	//Ponemos la operacionClicada a true para indicarle a la
	//funcion ponerNum, que debe hacer como he explicado anteriormente.
	operacionClicada = true;

	//Si el boton clicado no es el =
	//entonces debemos realizar las operaciones de un solo numero
	if(operacion != '=') {
		num = parseFloat(document.getElementById("imput").value);
		signo = operacion;
		switch (operacion) { //Switch que controla que operacion hemos clicado
			case '%':
				calculo = num / 100;
				break;
			case '1/x':
				calculo = 1 / num;
				break;
			case 'raiz':
				calculo = Math.sqrt(num);
				break;
			default:
				calculo = document.getElementById("imput").value;
		}
		document.getElementById("imput").value = calculo;

	} else {
		//Si el boton clicado es el = realizaremos la popercación pertinente
		switch (signo) {
			case '+':
				calculo = num + parseInt(document.getElementById("imput").value);
				break;
			case '-':
				calculo = num - parseInt(document.getElementById("imput").value);
				break;
			case '*':
				calculo = num * parseInt(document.getElementById("imput").value);
				break;
			case '/':
				calculo = num / parseInt(document.getElementById("imput").value);
				break;
			default:
				text = "No value found";
		}
	
		//Mostramo el resultado del calculo realizado
		document.getElementById("imput").value = calculo;
	}
}

