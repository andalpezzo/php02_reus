var texto = prompt("Escribe un texto");
var textoSinEspacios = texto.replace(/ /g, "");

if(textoSinEspacios == textoSinEspacios.toUpperCase()) {
    alert("El texto está formado sólo por mayúsculas");
} else if(textoSinEspacios == textoSinEspacios.toLowerCase()) {
    alert("El texto está formado sólo por minúsculas");
} else {
    alert("El texto está formado por mayúsculas y minúsculas");
}

