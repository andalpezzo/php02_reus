<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            //Declaramos las variables
            $numero = 8; //Variable tipo integer.
            $numeroDecimal = 8.8; //Variable tipo double.
            $nombre = "Andrea"; //Variable tipo string
            $esTrue = true; //Variable tipo boolean.

            echo "Variable tipo integer: ";
            echo $numero."<br>";
            echo "Variable tipo double: ";
            echo $numeroDecimal."<br>";
            echo "Variable tipo string: ";
            echo $nombre."<br>";
            echo "Variable tipo boolean: ";
            echo $esTrue; 
        ?>
    </body>
</html>
