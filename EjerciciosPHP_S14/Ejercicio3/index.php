<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            //Declaramos la variable num con un random del uno al tres
            $num = rand(1,3);
            
            //Creamos condiciones depende del numero que sea, escribiremos un string o otro
            if ($num == 1) {
                echo "uno";
            } else if($num == 2) {
                echo "dos";
            } else {
                echo "tres";
            }
        ?>
    </body>
</html>
