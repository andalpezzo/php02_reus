<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            //Declaramos las variables
            $N = 5;
            $A = 4.56;
            $C = 'a';
            
            //Realizamos las operaciones
            echo "Variable N = " . $N . "<br>";
            echo "Variable A = " . $A . "<br>";
            echo "Variable C = " . $C . "<br>";
            echo $N . " + " . $A . " = " . ($N + $A) . "<br>";
            echo $A . " - " . $N . " = " . ($A - $N) . "<br>";
            echo "Valor numérico del carácter " . $C . " = " . (int)$C;
        ?>
    </body>
</html>
