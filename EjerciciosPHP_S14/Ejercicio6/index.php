<!DOCT$YPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            //Declaramos las variables
            $X = 1;
            $Y = 2;
            $M = 3.2;
            $N = 4.7;

            //Realizamos las operaciones
            echo "Variable X = " . $X . "<br>";
            echo "Variable Y = " . $Y . "<br>";
            echo "Variable M = " . $M . "<br>";
            echo "Variable N = " . $N . "<br>";
            echo $X . " + " . $Y . " = " . ($X + $Y) . "<br>";
            echo $X . " - " . $Y . " = " . ($X - $Y) . "<br>";
            echo $X . " * " . $Y . " = " . $X * $Y . "<br>";
            echo $X . " / " . $Y . " = " . $X / $Y . "<br>";
            echo $X . " % " . $Y . " = " . $X % $Y . "<br>";
            echo $N . " . " . $M . " = " . ($N + $M) . "<br>";
            echo $N . " - " . $M . " = " . ($N - $M) . "<br>";
            echo $N . " * " . $M . " = " . $N * $M . "<br>";
            echo $N . " / " . $M . " = " . $N / $M . "<br>";
            echo $N . " % " . $M . " = " . $N % $M . "<br>";
            echo $X . " . " . $N . " = " . ($X + $N) . "<br>";
            echo $Y . " / " . $M . " = " . $Y / $M . "<br>";
            echo $Y . " % " . $M . " = " . $Y % $M . "<br>";
            echo "El doble de " . $X . " es " . 2 * $X . "<br>";
            echo "El doble de " . $Y . " es " . 2 * $Y . "<br>";
            echo "El doble de " . $M . " es " . 2 * $M . "<br>";
            echo "El doble de " . $N . " es " . 2 * $N . "<br>";
            echo $X . " . " . $Y . " . " . $N . " . " . $M . " = " . ($X + $Y + $M + $N) . "<br>";
            echo $X . " * " . $Y . " * " . $N . " * " . $M . " = " . ($X * $Y * $M * $N);
        ?>
    </body>
</html>
