<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            //Declaramos las variables
            $N = 1;
            $M;
            
            echo "Valor inicial de N = " . $N . "<br>";
            
            //Asignamos los valores a M y realizamos las operaciones
            $M = 77;
            echo "N + " . $M . " = " . ($N += $M) . "<br>";
            
            $M = 3;
            echo "N - " . $M . " = " . ($N -= $M) . "<br>";
            
            $M = 2;
            echo "N * " . $M . " = " . ($N *= $M);
        ?>
    </body>
</html>
