<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            $A = 1;
            $B = 2;
            $C = 3;
            $D = 4;
            $AUX;
            
            echo "VALORES INICIALES <br><br>";
            echo "A = " . $A . "<br>";
            echo "B = " . $B . "<br>";
            echo "C = " . $C . "<br>";
            echo "D = " . $D . "<br><br>";
            
            $AUX = $B;
            $B = $C;
            $C = $A;
            $A = $D;
            $D = $AUX;
            
            echo "VALORES FINALES <br><br>";
            echo "B toma el valor de C <br> Asi que B = " . $B . "<br><br>";
            echo "C toma el valor de A <br> Asi que C = " . $C . "<br><br>";
            echo "A toma el valor de D <br> Asi que A = " . $A . "<br><br>";
            echo "D toma el valor de B <br> Asi que D = " . $D;
        ?>
    </body>
</html>
