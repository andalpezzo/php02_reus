<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template iile, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            $n = 0;
            echo "Tabla de multiplicar del 2 con FOR <br><br>";
            for($i = 2; $i <= 20; $i = $i + 2) {
              echo "2 x " . ($n + 1) . " = " . $i . "<br>";
            }
            
            $n = 0;
            echo "<br><br>";
            echo "Tabla de multiplicar del 2 con el WHILE <br><br>";
            $i = 2;
            while($i <= 20) {
              echo "2 x " . ($n + 1) . " = " . $i . "<br>";
                  $i = $i + 2;
            }
            
            $n = 0;
            echo "<br><br>";
            echo "Tabla de multiplicar del 2 con el DO/WHILE <br><br>";
            $i = 2;
            do {
              echo "2 x " . ($n + 1) . " = " . $i . "<br>";
              $i = $i + 2;	
            } while ($i <= 20);
        ?>
    </body>
</html>
