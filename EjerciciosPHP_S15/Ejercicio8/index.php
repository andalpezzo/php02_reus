<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        //Declaramos las variables
        $num1 = 20;
        $num2 = 20;
 
        //Hacemos la comprobación
        if ($num1 >= $num2) {
            //If anidado
            if($num1 == $num2) {
                echo "El número " . $num1 . " y el número " . $num2 . " son iguales";
            } else {
                echo "El número " . $num1 . " es mayor que el número " . $num2;
            }
        } else {
            echo "El número " . $num2 . " es mayor que el número " . $num1;
        }
        ?>
    </body>
</html>
