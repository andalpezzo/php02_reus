<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            abstract class Operacion {
                protected $valor1;
                protected $valor2;
                protected $result;
                
                public function cargar1($val) {
                    $this -> valor1 = $val;
                }
                
                public function cargar2($val) {
                    $this -> valor2 = $val;
                }
                
                public function showResult() {
                    echo $this -> result;
                }

                public abstract function operar();
            }

            class Suma extends Operacion {
                public function operar() {
                    $this -> result = $this -> valor1 + $this -> valor2;
                }
            }

            class Resta extends Operacion {
                public function operar() {
                    $this -> result = $this -> valor1 - $this -> valor2;
                }
            }

            $suma=new Suma();
            $suma->cargar1(5);
            $suma->cargar2(8);
            $suma->operar();
            echo 'El resultado de la suma es: ';
            $suma->showResult();
            $resta=new Resta();
            $resta->cargar1(20);
            $resta->cargar2(4);
            $resta->operar();
            echo '<br>El resultado de la resta es: ';
            $resta->showResult();
        ?>
    </body>
</html>
