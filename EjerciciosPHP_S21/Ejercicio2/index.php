<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        class Persona {
            protected $nombre;
            protected $edad;
            
            public function datosPersonales($nombre,$edad) {
              $this->nombre=$nombre;
              $this->edad=$edad;
            }
            
            public function mostrarDatosPersonales() {
              echo 'Nombre:' . $this-> nombre . '<br>';
              echo 'Edad:'. $this-> edad . '<br>';
            }
          }

          class Empleado extends Persona{
            protected $sueldo;
            public function cargarSueldo($sueldo) {
              $this -> sueldo = $sueldo;
            }
            
            public function imprimirSueldo() {
              echo 'Sueldo:' . $this -> sueldo . '<br>';
            }
          }

          $persona1 = new Persona();
          $persona1 -> datosPersonales('Andrea Dal Pezzo', 22);
          echo 'Datos personales persona<br><br>';
          $persona1 -> mostrarDatosPersonales();
          $empleado1 = New Empleado();
          $empleado1 -> datosPersonales('Raúl Dal Pezzo',30);
          $empleado1 -> cargarSueldo(1500);
          echo '<br><br>Datos personales empleado<br><br>';
          $empleado1 -> mostrarDatosPersonales();
          echo '<br><br>Sueldo del empleado<br><br>';
          $empleado1 -> imprimirSueldo();
        ?>
    </body>
</html>
