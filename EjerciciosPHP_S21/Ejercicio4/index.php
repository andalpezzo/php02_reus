<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            class Persona {
                protected $nombre;
                protected $edad;
                
                public function cargarDatosPersonales($nombre, $edad) {
                  $this -> nombre = $nombre;
                  $this -> edad = $edad;
                }
                
                public function imprimirDatosPersonales() {
                  echo 'Nombre:'. $this -> nombre . '<br>';
                  echo 'Edad:' . $this -> edad . '<br>';
                }
              }

              class Empleado extends Persona{
                protected $sueldo;
                
                public function cargarSueldo($sueldo) {
                  $this -> sueldo = $sueldo;
                }
                
                public function imprimirSueldo() {
                  echo 'Sueldo:' . $this -> sueldo . '<br>';
                }
              }

            $empleado1 = New Empleado();
            $empleado1 -> edad = 34;
        ?>
    </body>
</html>
