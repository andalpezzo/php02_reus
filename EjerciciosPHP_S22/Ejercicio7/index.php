<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            class Operacion {
                protected $valor1;
                protected $valor2;
                protected $result;

                public function __construct($val1,$val2) {
                  $this -> valor1 = $val1;
                  $this -> valor2 = $val2;
                }

                public function showResult() {
                  echo $this -> resultado . '<br>';
                }
            }

            class Suma extends Operacion {
                protected $titulo;
                public function __construct($val1, $val2, $tit) {
                  Operacion::__construct($val1,$val2);
                  $this -> titulo = $tit;
                }

                public function operar() {
                  echo $this -> titulo;
                  echo $this -> valor1 . ' + ' . $this -> valor2 . ' es igual a ';
                  $this -> resultado = $this -> valor1 + $this -> valor2;
                }
            }

        $suma=new Suma(10, 10, 'La suma de ');
        $suma->operar();
        $suma->showResult();
        ?>
    </body>
</html>
