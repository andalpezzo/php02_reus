/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

addEventListener('load',inicializarEventos,false);

function inicializarEventos() {
  var ref = document.getElementById('votofoto');
  var vec = ref.getElementsByTagName('li');
  var vec2 = ref.getElementsByTagName('a');
  for(var f = 0; f < vec2.length;f++)
  {
    vec[f].addEventListener('mouseover', entrar, false);
    vec[f].addEventListener('mouseout', salir, false);
    vec2[f].addEventListener('click', presionBoton, false);
  }
}

function entrar(e) {
  var ref = e.target;
  var ob = document.getElementById('votofoto');
  var vec = ob.getElementsByTagName('li');
  for(var f = 0; f < ref.firstChild.nodeValue; f++) {
    vec[f].firstChild.style.background='#f00';
    vec[f].firstChild.style.color='#fff';
  }    
}

function salir(e) {
  var ref = e.target;
  var ob = document.getElementById('votofoto');
  var vec = ob.getElementsByTagName('li');
  for(var f = 0; f < ref.firstChild.nodeValue; f++) {
    vec[f].firstChild.style.background='#f7f8e8';
    vec[f].firstChild.style.color='#f00';
  }  
}

function presionBoton(e) {
  e.preventDefault();
  var ref = e.target;
  cargarVoto(ref.firstChild.nodeValue);
}

var conexion;
function cargarVoto(voto) {
  conexion = new XMLHttpRequest(); 
  conexion.onreadystatechange = procesarEventos;
  var aleatorio = Math.random();
  conexion.open('GET', 'pagina1.php?voto=' + voto + "&aleatorio=" + aleatorio, true);
  conexion.send();
}

function procesarEventos() {
  var resultados = document.getElementById("resultados");
  if(conexion.readyState == 4) {
    resultados.innerHTML = 'Gracias.';
  } else {
    resultados.innerHTML = 'Procesando...';
  }
}

