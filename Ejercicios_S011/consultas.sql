CREATE DATABASE estaciones;
USE estaciones;
CREATE TABLE estaci�n(id INT AUTO_INCREMENT PRIMARY KEY, latitud DOUBLE, longitud DOUBLE, al
titud DOUBLE);
CREATE TABLE muestra (id INT NOT NULL REFERENCES estaci�n (id), fecha DATE, tempMin DOUBLE, tempMax DOUBLE, precipitaciones DOUBLE, humedadMin DOUBLE, humedadMax DOUBLE, velocidadVientoMin DOUBLE, velocidadVientoMax DOUBLE);

CREATE DATABASE biblioteca;                                                                               
USE biblioteca;                                                                        
CREATE TABLE editorial (claveEditorial SMALLINT NOT NULL, nombre VARCHAR(60), direccion VARCHAR (60), telefono VARCHAR(15), PRIMARY KEY (claveEditorial));        
CREATE TABLE libro (claveLibro INT NOT NULL, titulo VARCHAR (60), idioma VARCHAR(15), formato VARCHAR(15), claveEditorial SMALLINT, PRIMARY KEY(claveLibro), KEY(claveEditorial), FOREIGN KEY(claveEditorial) REFERENCES editorial(claveEditorial) ON DELETE SET NULL ON UPDATE CASCADE);
CREATE TABLE tema (claveTema SMALLINT NOT NULL, nombre VARCHAR(40), PRIMARY KEY(claveTema));
CREATE TABLE autor (claveAutor INT NOT NULL, nombre VARCHAR(60), PRIMARY KEY (claveAutor));
CREATE TABLE ejemplar (claveEjemplar INT NOT NULL, claveLibro INT NOT NULL, numeroOrden SMALLINT NOT NULL, edicion SMALLINT, ubicacion VARCHAR(15), categoria CHAR, PRIMARY KEY (claveEjemplar), FOREIGN KEY (claveLibro) REFERENCES libro(claveLibro) ON DELETE CASCADE ON UPDATE CASCADE); 
CREATE TABLE socio (claveSocio INT NOT NULL, nombre VARCHAR(60), direccion VARCHAR(60), telefono VARCHAR(15), categoria CHAR, PRIMARY KEY (claveSocio));
CREATE TABLE prestamo (claveSocio INT, claveEjemplar INT, numeroOrden SMALLINT, fecha_prestamo DATE NOT NULL, fecha_devolucion DATE DEFAULT NULL, notas BLOB, FOREIGN KEY (claveSocio) REFERENCES socio(claveSocio) ON DELETE SET NULL ON UPDATE CASCADE, FOREIGN KEY (claveEjemplar) REFERENCES ejemplar(claveEjemplar) ON DELETE SET NULL ON UPDATE CASCADE);
CREATE TABLE trata_sobre (claveLibro INT NOT NULL, claveTema SMALLINT NOT NULL, FOREIGN KEY (claveLibro) ON DELETE CASCADE ON UPDATE CASCADE, FOREIGN KEY (claveTema) REFERENCES tema(claveTema) ON DELETE CASCADE ON UPDATE CASCADE);	